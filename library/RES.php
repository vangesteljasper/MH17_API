<?php

class RES {

  /**
   * sends an array as response and sets the status code
   *
   * @param array $responseData
   * @param int   $statusCode
   * @return void
   */
  static public function send(array $responseData, int $statusCode): void
  {
    header('Content-Type: application/json');
    http_response_code($statusCode);

    echo json_encode($responseData);
    exit();
  }

  /**
   * sends a message as response and sets the status code
   *
   * @param string $infoMessage
   * @param int    $statusCode
   * @return void
   */
  static public function sendMessage(string $infoMessage, int $statusCode): void
  {
    $responseData = ['message' => $infoMessage];
    RES::send($responseData, $statusCode);
  }

  /**
   * sends a null response and sets the status code
   *
   * @return void
   */
  static public function sendCode(int $statusCode): void
  {
    http_response_code($statusCode);
    exit();
  }

}
