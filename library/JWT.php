<?php

use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;

class JWT {

  /**
   * varifies a token and returns an array representing the user
   * from the token if successfully validated
   *
   * @param string|null                      $stringToken
   * @param \Lcobucci\JWT\Signer\Hmac\Sha256 $signer
   * @param string                           $secret
   * @return array
   */
  static public function verifyUser(
    string $stringToken = null,
    Lcobucci\JWT\Signer\Hmac\Sha256 $signer,
    string $secret
  ): array
  {
    if ($stringToken === null) RES::sendCode(401);

    $token = JWT::parseToken($stringToken);
    $result = JWT::validateToken($token, $signer, $secret);

    if ($result['valid']) { // if valid token
      $user = JWT::getTokenUser($token);
      return $user;
    }
    else { // token NOT valid
      RES::sendMessage($result['reason'], 401);
    }
  }

  /**
   * converts a string token into a token object
   *
   * @param string $stringToken
   * @return \Lcobucci\JWT\Token
   */
  static public function parseToken(string $stringToken)
  {
    $parser = new Parser();
    $token = $parser->parse($stringToken);
    return $token;
  }

  /**
   * check if the token is unchanged and still valid
   *
   * @param \Lcobucci\JWT\Token              $token
   * @param \Lcobucci\JWT\Signer\Hmac\Sha256 $signer
   * @param string                           $secret
   * @return array
   */
  static public function validateToken(
    Lcobucci\JWT\Token $token,
    Lcobucci\JWT\Signer\Hmac\Sha256 $signer,
    string $secret
  ): array
  {
    // check secret
    $unaltered = $token->verify($signer, $secret);

    if ($unaltered) {

      // validate fields
      $data = new ValidationData();
      $valid = $token->validate($data);

      if ($valid) {
        return ['valid' => true];
      } else {
        return [
          'valid' => false,
          'reason' => 'Token Expired'
        ];
      }

    } else {
      return [
        'valid' => false,
        'reason' => 'Token Invalid'
      ];
    }
  }

  /**
   * create a token containing a user array
   *
   * @param \Lcobucci\JWT\Signer\Hmac\Sha256 $signer
   * @param string                           $secret
   * @param string                           $key
   * @param array                            $data
   * @return string
   */
  static public function createToken(
    Lcobucci\JWT\Signer\Hmac\Sha256 $signer,
    string $secret,
    string $key,
    array $data
  ): string
  {
    $time = time();

    $token = (new Builder())
      ->setIssuer('https://maartenh.com')
      ->setAudience('https://maartenh.com')
      ->setIssuedAt($time)
      ->setExpiration($time + (60 * 20)) // 20 minutes
      ->set($key, $data)
      ->sign($signer, $secret)
      ->getToken();

    return (String) $token;
  }

  /**
   * get the user data out of a token
   *
   * @param \Lcobucci\JWT\Token $token
   * @return array
   */
  static public function getTokenUser(Lcobucci\JWT\Token $token): array
  {
    $user = $token->getClaim('user');

    return [
      'name' => $user->username,
      'id' => $user->_id
    ];
  }

}
