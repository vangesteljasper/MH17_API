<?php

class MISC {

  /**
   * generates a random uuidv4
   *
   * @return string
   */
  static public function uuidv4(): string
  {
    $data = random_bytes(16);
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }

  /**
   * sanitize user input arrays
   *
   * @param array $data
   * @return array
   */
  static public function sanitize(array $data): array
  {
    $result = [];

    foreach ($data as $key => $value) {
      $key = trim(strip_tags($key));
      $result[$key] = trim(strip_tags($value));
    }

    return $result;
  }

  /**
   * sanitize user input strings
   *
   * @param string $string
   * @return string
   */
  static public function sanitizeString(string $string): string
  {
    return trim(strip_tags($string));
  }

}
