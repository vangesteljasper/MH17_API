<?php

// initialise error reporting
// development only
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Authorization');

$METHOD = $_SERVER['REQUEST_METHOD']; // get method

// send 200 response on OPTIONS preflight request
if ($METHOD === 'OPTIONS') {
  http_response_code(200);
  exit();
}

$URL = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; // get full url
$PATH = ltrim(rtrim(parse_url($URL, PHP_URL_PATH), '/'), '/'); // get path without leading and trailing slashes
$PATH_PARTS = explode('/', $PATH); // get array of parts of path
$QUERY = parse_url($URL, PHP_URL_QUERY); // get query from url

if (strpos($PATH_PARTS[0], "api") !== 0): exit(); endif; // exit if route doesn't start with 'api'
if (count($PATH_PARTS) < 2): exit(); endif; // exit if route doesn't contain anything after 'api'

// some setup
define('DS', DIRECTORY_SEPARATOR);
define('WWW_ROOT', __DIR__ . DS);

require_once WWW_ROOT . 'CONFIG.php';

// if route is in routes array
if (array_key_exists($PATH_PARTS[1], ROUTES)) {
  try {
    $config = ROUTES[$PATH_PARTS[1]];
    $modelObj = null;

    // load daos
    foreach (DAOS as $dao) {
      require_once WWW_ROOT . 'dao' . DS . "{$dao}.php";
    }

    // load libraries
    foreach (LIBS as $lib) {
      require_once WWW_ROOT . 'library' . DS . "{$lib}.php";
    }

    // controller stuff
    $controller = $config['controller'];
    require_once WWW_ROOT . 'controllers' . DS . 'Controller.php';
    require_once WWW_ROOT . 'controllers' . DS . "{$controller}.php";

    // model stuff
    $model = $config['model'];
    if ($model !== null) {
      require_once WWW_ROOT . 'models' . DS . 'Model.php';
      require_once WWW_ROOT . 'models' . DS . "{$model}.php";

      $modelObj = new $model();
    }

    // initiate controller
    new $controller($PATH_PARTS, $QUERY, $modelObj);

  } catch (Exception $e) {
    RES::sendWithMessage($e->getMessage(), 500);
  }
}

exit();

// TODO: add filter in query (eg: title=mech)
// TODO: check types with model instead of always string
