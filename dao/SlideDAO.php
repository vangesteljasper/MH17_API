<?php

require_once WWW_ROOT . 'dao' . DS . 'DAO.php';

class SlideDAO extends DAO {

  protected $defaultFields;
  protected $defaultSort;

  /**
   * SlideDAO constructor
   *
   * @param string $fields
   * @param string $sort
   */
  function __construct(string $fields, string $sort)
  {
    $this->defaultFields = $fields;
    $this->defaultSort = $sort;

    parent::__construct();
  }

  /**
   * get all slides
   *
   * @param string $fields
   * @param string $sort
   * @return array|null
   */
  public function getAll(string $fields, string $sort): ?array
  {
    $sql = "SELECT {$fields} FROM `mh17_slides` WHERE `_active` = 1 ORDER BY {$sort}";
		$stmt = $this->pdo->prepare($sql);
		if ($stmt->execute()) {
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
		return null;
  }

  /**
   * get a slide by id
   *
   * @param string $id
   * @param string $fields
   * @param string $sort
   * @return array|null
   */
  public function getById(string $id, string $fields, string $sort): ?array
  {
    $sql = "SELECT {$fields} FROM `mh17_slides` WHERE `_id` = :id AND `_active` = 1 ORDER BY {$sort}";
    $stmt = $this->pdo->prepare($sql);

    $stmt->bindValue(':id', $id);

    if ($stmt->execute()) {
      return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    return null;
  }

  /**
   * insert a slide
   *
   * @param array $fields
   * @return array|null
   */
  public function insert(array $fields): ?array
  {
    $title = $fields['title'];
    $description = $fields['description'];
    $link = $fields['link'];
    $image = $fields['image'];

    $id = MISC::uuidv4();
    $now = (new DateTime())->format('Y-m-d H:i:s');

    $sql = "INSERT INTO `mh17_slides` (`_id`, `_active`, `title`, `link`, `description`, `image`, `_created`, `_modified`) VALUES (:id, :active, :title, :link, :description, :image, :created, :modified)";
		$stmt = $this->pdo->prepare($sql);

    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':active', 1);
    $stmt->bindValue(':created', $now);
    $stmt->bindValue(':modified', $now);

    $stmt->bindValue(':title', $title);
    $stmt->bindValue(':link', $link);
    $stmt->bindValue(':description', $description);
    $stmt->bindValue(':image', $image);

    if ($stmt->execute()) {
      return $this->getById($id, $this->defaultFields, $this->defaultSort);
    }
    return null;
  }

  /**
   * check if a slide exists or not
   *
   * @param string $id
   * @return bool
   */
  public function exists(string $id): bool
  {
    $sql = "SELECT COUNT(`_id`) as `count` FROM `mh17_slides` WHERE `_id` = :id AND `_active` = 1";

    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $id);

    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC)['count'];
    if ($result == 1) {
      return true;
    }
    return false;
  }

  /**
   * update a post
   *
   * @param string $id
   * @param array  $data
   * @return array|null
   */
  public function update(string $id, array $data): ?array
  {
    $queryFields = $this->createQueryFieldsString($data);

    $sql = "UPDATE `mh17_slides` SET {$queryFields} WHERE `_id` = :id";

    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $id);

    if ($stmt->execute()) {
      return $this->getById($id, $this->defaultFields, $this->defaultSort);
    }
    return null;
  }

  /**
   * remove a slide
   *
   * @param string $id
   * @return bool
   */
  public function remove(string $id): bool
  {
    $data = ['_active' => 0];
    $queryFields = $this->createQueryFieldsString($data);

    $sql = "UPDATE `mh17_slides` SET {$queryFields} WHERE `_id` = :id";

    $stmt = $this->pdo->prepare($sql);
    $stmt->bindValue(':id', $id);

    return $stmt->execute();
  }

}
