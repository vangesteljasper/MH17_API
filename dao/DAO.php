<?php

class DAO {
	private static $dbHost = "localhost";
	private static $dbName = "mh17";
	private static $dbUser = "root";
	private static $dbPass = "root";
	private static $sharedPDO;
	protected $pdo;

  /**
   * DAO constructor
   */
  function __construct()
  {
		if(empty(self::$sharedPDO)) {
			self::$sharedPDO = new PDO("mysql:host=" . self::$dbHost . ";dbname=" . self::$dbName, self::$dbUser, self::$dbPass);
			self::$sharedPDO->exec("SET CHARACTER SET utf8");
			self::$sharedPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$sharedPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		$this->pdo =& self::$sharedPDO;
	}

  /**
   * returns a string for use in a sql statement
   * containing the keys and values you pass with this method
   * @param array $data
   * @return string
   */
  protected function createQueryFieldsString(array $data): string
	{
		$list = [];
    foreach ($data as $key => $value) {
      $value = json_encode($value);
      $list[] = "`{$key}` = {$value}";
    }
    $list[] = "`_modified` = NOW()";

		return join(', ', $list);
	}
}
