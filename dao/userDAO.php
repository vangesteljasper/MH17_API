<?php

require_once WWW_ROOT . 'dao' . DS . 'DAO.php';
require_once WWW_ROOT . 'vendor' . DS . 'phpass' . DS . 'Phpass.php';

class UserDAO extends DAO {

  private $hasher;
  public $test;

  /**
   * UserDAO constructor
   */
  function __construct() {
    parent::__construct();
    $this->hasher = new Phpass\Hash();
  }

  /**
   * validates a username and password against the database
   *
   * @param string $username
   * @param string $testPassword
   * @return array|null
   */
  public function validate(string $username, string $testPassword): ?array
  {
    $sql = "SELECT `_id`, `username`, `password` FROM `mh17_users` WHERE `username` = :username";
    $stmt = $this->pdo->prepare($sql);

		$stmt->bindValue(':username', $username);
		$stmt->execute();

		$user = $stmt->fetch(PDO::FETCH_ASSOC);
    $validPassword = $user['password'];

    $valid = $this->hasher->checkPassword($testPassword, $validPassword);

		if ($valid) {
			return [
        'id' => $user['_id'],
        'name' => $user['username']
      ];
		}
		return null;
  }

}
