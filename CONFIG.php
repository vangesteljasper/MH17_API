<?php

$ROUTES = [
  'posts' => [
    'controller' => 'PostController',
    'model' => 'PostModel'
  ],
  'auth' => [
    'controller' => 'AuthController',
    'model' => 'UserModel'
  ],
  'me' => [
    'controller' => 'MeController',
    'model' => null
  ],
  'slides' => [
    'controller' => 'SlideController',
    'model' => 'SlideModel'
  ]
];

$DAOS = [
  'PostDAO',
  'UserDAO',
  'SlideDAO'
];

$LIBS = [
  'JWT',
  'MISC',
  'RES'
];

define('ROUTES', $ROUTES);
define('DAOS', $DAOS);
define('LIBS', $LIBS);
