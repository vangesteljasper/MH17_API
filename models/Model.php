<?php

class Model {

  /**
   * get the required fields of the model
   *
   * @return array|null
   */
  public function getRequiredFields(): ?array
  {
    $requiredFields = [];
    $fields = get_object_vars($this);
    foreach ($fields as $key => $value) {
      if ($value['required']) {
        $requiredFields[] = $key;
      }
    }
    if (!empty($requiredFields)) {
      return $requiredFields;
    }
    return null;
  }

  public function getOptionalFields(): ?array
  {
    $optionalFields = [];
    $fields = get_object_vars($this);
    foreach ($fields as $key => $value) {
      if (!$value['required']) {
        $optionalFields[] = $key;
      }
    }
    if (!empty($optionalFields)) {
      return $optionalFields;
    }
    return null;
  }

  /**
   * get all the fields of the model
   *
   * @return array
   */
  public function getAllFields(): array
  {
    return get_object_vars($this);
  }

  public function __toString(): string
  {
    $fields = $this->getAllFields();
    $result = [];
    foreach ($fields as $key => $value) {
      $type = $value['type'];
      $required = $value['required'] ? 'required' : 'optional';
      $result[] = "'{$key}: {$type} ({$required})'";
    }
    $result = join(', ', $result);
    return "[{$result}]";
  }
}
