<?php

class PostModel extends Model {

  public $title;
  public $description;
  public $date;
  public $image;

  /**
   * PostModel constructor
   */
  function __construct()
  {
    $this->title = [
      'type' => 'string',
      'required' => true
    ];
    $this->description = [
      'type' => 'string',
      'required' => false
    ];
    $this->date = [
      'type' => 'string',
      'required' => false
    ];
    $this->image = [
      'type' => 'string',
      'required' => true
    ];
  }

}
