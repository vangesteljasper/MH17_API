<?php

class SlideModel extends Model {

  public $title;
  public $description;
  public $link;
  public $image;

  /**
   * SlideModel constructor
   */
  function __construct()
  {
    $this->title = [
      'type' => 'string',
      'required' => true
    ];
    $this->description = [
      'type' => 'string',
      'required' => false
    ];
    $this->link = [
      'type' => 'string',
      'required' => false
    ];
    $this->image = [
      'type' => 'string',
      'required' => true
    ];
  }

}
