<?php

class SlideController extends Controller {

  protected $slideDAO;

  /**
   * SlideController constructor
   *
   * @param array       $route
   * @param string|null $query
   * @param \Model      $model
   */
  function __construct(array $route, string $query = null, Model $model)
  {
    $defaultFields = "`_id`, `title`, `description`, `link`, `image`";
    $defaultSort = "`_created` ASC";

    $this->slideDAO = new SlideDAO($defaultFields, $defaultSort);

    parent::__construct($route, $query, $model, $defaultFields, $defaultSort);
  }

  /**
   * method for handling GET requests
   *
   * @return void
   */
  protected function GET(): void
  {
    $fields = $this->query['fields'];
    $sort = $this->query['sort'];

    $slides = $this->slideDAO->getAll($fields, $sort);
    $response = ['slides' => $slides];
    RES::send($response, 200);
  }

  /**
   * method for handling GET requests with id
   *
   * @return void
   */
  protected function GET_SINGLE(): void
  {
    if (!$this->slideDAO->exists($this->routeId)) RES::sendCode(404);

    $fields = $this->query['fields'];
    $sort = $this->query['sort'];

    $slide = $this->slideDAO->getById($this->routeId, $fields, $sort);
    $response = ['slide' => $slide];
    RES::send($response, 200);
  }

  /**
   * method for handling POST requests
   *
   * @return void
   */
  protected function POST(): void
  {
    $this->authenticate();
    $this->fallbackOptionalPayloadFields();
    $this->validatePayload();

    $slide = $this->slideDAO->insert($this->payload);
    $response = ['slide' => $slide];
    RES::send($response, 201);
  }

  /**
   * method for handling PATCH requests
   *
   * @return void
   */
  protected function PATCH(): void
  {
    $this->__UPDATE();
  }

  /**
   * method for handling PUT requests
   *
   * @return void
   */
  protected function PUT(): void
  {
    $this->__UPDATE();
  }

  /**
   * method for handling GET requests
   *
   * @return void
   */
  protected function DELETE(): void
  {
    $this->authenticate();
    if (!$this->slideDAO->exists($this->routeId)) RES::sendCode(404);
    if ($this->slideDAO->remove($this->routeId)) RES::sendCode(204);
    RES::sendCode(500);
  }

  /**
   * mechanism for updating a post
   * for use in eg PATCH and PUT requests
   *
   * @return void
   */
  protected function __UPDATE(): void
  {
    $this->authenticate();
    if (!$this->slideDAO->exists($this->routeId)) RES::sendCode(404);
    $this->validateFields();

    $slide = $this->slideDAO->update($this->routeId, $this->fields);

    if ($slide) {
      $response = ['slide' => $slide];
      RES::send($response, 200);
    } else {
      RES::sendCode(500);
    }
  }

}
