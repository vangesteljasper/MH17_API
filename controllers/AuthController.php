<?php

class AuthController extends Controller {

  private $userDAO;

  /**
   * AuthController constructor
   *
   * @param array       $route
   * @param string|null $query
   * @param \Model      $model
   */
  function __construct(array $route, string $query = null, Model $model)
  {
    $this->userDAO = new UserDAO();
    parent::__construct($route, $query, $model);
  }

  /**
   * method for handling POST request
   *
   * @return void
   */
  protected function POST(): void
  {
    $this->validatePayload();

    $username = $this->payload['username'];
    $password = $this->payload['password'];

    // validate user
    $user = $this->userDAO->validate($username, $password);
    if ($user !== null) {
      $token = JWT::createToken($this->signer, $this->secret, 'user', $user);
      $response = ['token' => $token];
      RES::send($response, 200);
    } else {
      RES::sendCode(401);
    }

  }

}
