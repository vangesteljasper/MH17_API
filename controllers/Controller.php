<?php

require_once WWW_ROOT . 'vendor' . DS . 'autoload.php';
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Controller {

  protected $route, $model, $query, $method, $fields, $routeId, $secret, $token, $signer, $defaultFields, $defaultSort;
  private $uuidv4regex;

  /**
   * Controller constructor
   *
   * @param array       $route
   * @param string|null $query
   * @param \Model|null $model
   * @param string|null $defaultFields
   * @param string|null $defaultSort
   */
  function __construct(
    array $route,
    string $query = null,
    Model $model = null,
    string $defaultFields = null,
    string $defaultSort = null
  )
  {
    $this->route = $route;
    $this->model = $model;

    $this->defaultFields = $defaultFields;
    $this->defaultSort = $defaultSort;

    $this->method = $_SERVER['REQUEST_METHOD'];
    $this->secret = 'IM7E3>Du%*6[/Dq164*2%/v5K%)q73';
    $this->signer = new Sha256();
    $this->uuidv4regex = '/^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i';

    $this->query = $this->buildQuery($query);
    $this->payload = $this->buildPayload();
    $this->routeId = $this->buildRouteId();
    $this->token = $this->buildToken();

    $this->init();
  }

  /**
   * execute the required method in subclass for the request
   *
   * @return void
   */
  private function init(): void
  {
    $handler = $this->method;

    if ($this->hasValidId()) {
      if ($this->method === 'GET') {
        $handler = "{$this->method}_SINGLE";
      }
    } else {
      $needId = ['PUT', 'PATCH'];
      if (in_array($this->method, $needId)) {
        RES::sendCode(404);
      }
    }

    // if handler exists in class (subclass) then use it
    // eg: POST(); GET_SINGLE();
    if (method_exists($this, $handler)) {
      // execute handler for this route
      $this->$handler();
    } else {
      RES::sendCode(404);
    }
  }

  /**
   * parses the raw request query and builds the needed strings for
   * the database
   *
   * @param string|null $rawQuery
   * @return array
   */
  private function buildQuery(string $rawQuery = null): array
  {
    $builtQuery = [
      'fields' => $this->defaultFields,
      'sort' => $this->defaultSort
    ];

    // return if no query is passed
    if ($rawQuery === null) {
      return $builtQuery;
    }

    // parse query into parsedQuery variable
    parse_str($rawQuery, $parsedQuery);

    // sanitize both keys and values from query
    $parsedQuery = MISC::sanitize($parsedQuery);

    $reservedKeys = ['fields', 'sort'];

    foreach ($parsedQuery as $key => $value) {

      // if the value is empty OR
      // if key is not in reservedKeys
      if ($value === '' || !in_array($key, $reservedKeys)) continue;

      // execute function for reserved key
      // eg $this->buildQueryFields($value);
      $upperKey = ucfirst($key);
      $func = "buildQuery{$upperKey}";
      // insert the result of the function into $builtQuery
      $builtQuery[$key] = $this->$func($value);
    }

    return $builtQuery;
  }

  /**
   * builds a string with fields to fetch from the database
   * and returns it
   *
   * @param $rawQueryValue
   * @return null|string
   */
  private function buildQueryFields($rawQueryValue): ?string
  {
    // turn comma seperated list into array
    $valueList = explode(',', $rawQueryValue);

    $validFields = $this->model->getAllFields();
    $builtFields = [];

    foreach ($valueList as $item) {

      // if the value is empty skip iteration OR
      // if the value is not in validKeys skip iteration
      if ($item === '' || !array_key_exists($item, $validFields)) continue;

      // add item to the builtFields array
      $builtFields[] = "`{$item}`";
    }

    // if nothing is in the builtFields array return default fields
    if (count($builtFields) === 0) return $this->defaultFields;

    // return a comma separated list (string) of the builtFields array
    return join(', ', $builtFields);
  }

  /**
   * builds a string for sorting database results
   * and returns it
   *
   * @param $rawQueryValue
   * @return null|string
   */
  private function buildQuerySort($rawQueryValue): ?string
  {
    $direction = 'ASC';
    $validFields = $this->model->getAllFields();

    // if the value starts with a - inverse direction
    if (strpos($rawQueryValue, '-') === 0) {
      $direction = 'DESC';
      $rawQueryValue = ltrim($rawQueryValue, '-');
    }

    // if the value is a valid field of the model
    // use it als sorting field
    if (array_key_exists($rawQueryValue, $validFields)) {
      return "`{$rawQueryValue}` {$direction}";
    }

    // return default sort
    return $this->defaultSort;
  }

  /**
   * sets fields parameter
   *
   * @return null|array
   */
  private function buildPayload(): ?array
  {
    if ($this->method == 'POST') {
      return MISC::sanitize($_POST);
    }
    if ($this->method == 'PUT' || $this->method == 'PATCH') {
      parse_str(file_get_contents("php://input"), $payload);
      return MISC::sanitize($payload);
    }
    return null;
  }

  /**
   * sets routeId parameter
   *
   * @return null|string
   */
  private function buildRouteId(): ?string
  {
    if (count($this->route) >= 3) {
      return MISC::sanitizeString($this->route[2]);
    }
    return null;
  }

  /**
   * sets token parameter
   *
   * @return null|string
   */
  private function buildToken(): ?string
  {
    if (!isset($_SERVER['HTTP_AUTHORIZATION'])) return null;
    if (strpos($_SERVER['HTTP_AUTHORIZATION'], 'Bearer ') !== 0) return null;

    $token = str_replace('Bearer ', '', $_SERVER['HTTP_AUTHORIZATION']);
    return MISC::sanitizeString($token);
  }

  /**
   * checks if the id after eg /posts or /slides is a valid uuidv4
   *
   * @return bool
   */
  private function hasValidId(): bool
  {
    // routeId !== null && routeId === uuidv4
    if ($this->routeId !== null && preg_match($this->uuidv4regex, $this->route[2])) {
      return true;
    }
    return false;
  }

  /**
   * alias for JWT::varifyUsers
   *
   * @return array
   */
  protected function authenticate(): array
  {
    return JWT::verifyUser($this->token, $this->signer, $this->secret);
  }

  /**
   * gets all the optional fields of the model and adds them to
   * the payload if the value is not initially set
   *
   * @return void
   */
  protected function fallbackOptionalPayloadFields(): void
  {
    $optionalFields = $this->model->getOptionalFields();
    foreach ($optionalFields as $field) {
      // if the valid field does not exist in payload
      if (!array_key_exists($field, $this->payload)) {
        $this->payload[$field] = '';
      }
    }
  }

  /**
   * validate fields from request against data model
   * and send error responses when needed
   *
   * @return void
   */
  protected function validatePayload(): void
  {
    $requiredFields = $this->model->getRequiredFields();
    $validFields = $this->model->getAllFields();

    // if not all required fields are in payload
    foreach ($requiredFields as $field) {
      if (!array_key_exists($field, $this->payload)) {
        RES::sendMessage("make sure the payload conforms to {$this->model}", 400);
      }
    }
    // if more fields are provided than defined in model
    foreach ($this->payload as $field => $value) {
      if (!array_key_exists($field, $validFields)) {
        RES::sendMessage("make sure the payload conforms to {$this->model}", 400);
      }
    }
  }
}
